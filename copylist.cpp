
//
// Задание: разработать программу для копирования списка файлов из одной директории в другую.
// Список файлов должен задаваться с помощью XML-файла произвольной структуры.
//
// Примечание: для реализации задачи нельзя использовать библиотеку Qt.
//

// 
// Windows Only реализация (с помощью библиотеки XmlLite).
// Плюсы данного решения: независимость от сторонних библиотек.
//
// Формат принимаемого XML-файла:
// <?xml version="1.0"?>
// <filelist>
//     <file name="filename" srcdir="srcdir" dstdir="dstdir" />
//     <!-- ... /-->
// </filelist>

#include <io.h>     // _setmode, _fileno
#include <fcntl.h>  // _O_U16TEXT

#include <atlbase.h>    // CComPtr, IStream
#include <xmllite.h>    // IXmlReader, CreateXmlReader

#include <string>
#include <iostream>

#pragma comment(lib, "xmllite.lib")

namespace {

bool ProcessFile(IXmlReader* reader)
{
    wchar_t* value = nullptr;

    if (FAILED(reader->MoveToAttributeByName(L"name", nullptr)))
        return false;

    if (FAILED(reader->GetValue(&value, nullptr)))
        return false;

    const std::wstring fname = value;

    if (FAILED(reader->MoveToAttributeByName(L"srcdir", nullptr)))
        return false;

    if (FAILED(reader->GetValue(&value, nullptr)))
        return false;

    const std::wstring srcdir = value;

    if (FAILED(reader->MoveToAttributeByName(L"dstdir", nullptr)))
        return false;

    if (FAILED(reader->GetValue(&value, nullptr)))
        return false;

    const std::wstring dstdir = value;

    wchar_t fsrc[MAX_PATH] = { L'\0' };
    if (!::PathCombineW(reinterpret_cast<LPWSTR>(&fsrc), srcdir.c_str(), fname.c_str()))
        return false;

    wchar_t fdst[MAX_PATH] = { L'\0' };
    if (!::PathCombineW(reinterpret_cast<LPWSTR>(&fdst), dstdir.c_str(), fname.c_str()))
        return false;

    BOOL rv = ::CopyFileW(fsrc, fdst, FALSE);
    if (rv) {
        std::wcout << L"File \"" << fname << L"\" copied from \"" << srcdir
                   << L"\" to \"" << dstdir << L"\"" << std::endl;
    }
    else {
        std::wcout << L"Error copying file \"" << fname << L"\" from \""
                   << srcdir << "\" to \"" << dstdir << "\"" << std::endl;
    }

    return rv;
}

} // namespace


int wmain(int argc, wchar_t* argv[])
{
    _setmode(_fileno(stdout), _O_U16TEXT);

    if (argc != 2) {
        std::wcout << L"Usage: CopyList <filelist.xml>" << std::endl;
        return 0;
    }

    CComPtr<IStream> stream = nullptr;
    CComPtr<IXmlReader> reader = nullptr;

    if (FAILED(::SHCreateStreamOnFileW(argv[1], STGM_READ, &stream)))
        return -1;

    if (FAILED(::CreateXmlReader(__uuidof(IXmlReader), reinterpret_cast<void**>(&reader), nullptr)))
        return -1;

    if (FAILED(reader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit)))
        return -1;

    if (FAILED(reader->SetInput(stream)))
        return -1;

    XmlNodeType nodeType;
    while (!reader->IsEOF()) {
        if (FAILED(reader->Read(&nodeType)))
            return -1;

        if (nodeType == XmlNodeType_Element) {
            wchar_t* nodeName = nullptr;
            if (FAILED(reader->GetLocalName(&nodeName, nullptr)))
                return -1;

            if (std::wcscmp(L"file", nodeName) == 0)
                ProcessFile(reader);
        }
    }

    return 0;
}
